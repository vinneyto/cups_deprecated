const fs = require('fs');const gulp = require('gulp');
const shell = require('gulp-shell');
const GulpSSH = require('gulp-ssh');
var clean = require('gulp-clean');

require('dotenv').config();

var config = {
    host: process.env.SSH_HOST,
    port: 22,
    username: process.env.SSH_USERNAME,
    privateKey: fs.readFileSync(process.env.SSH_KEY_PATH),
    passphrase: process.env.SSH_PASSPHRASE
};

var gulpSSH = new GulpSSH({
    ignoreErrors: false,
    sshConfig: config
});

gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('build', ['clean'], shell.task([
    'npm run build',
]));

gulp.task('prepare', ['build'], function () {
    return gulpSSH
        .shell([`cd ${process.env.UPLOAD_DIR}`, 'rm -rf *']);
});

gulp.task('deploy', ['prepare'], function () {
    return gulp.src('dist/**/*.*')
        .pipe(gulpSSH.dest(process.env.UPLOAD_DIR));
});
