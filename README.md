# cups frontend

### Local development

`cd` to project directory

Install dependencies (node 4.x.x and npm 3.x.x required)

    $ npm install

Then create file `.env` with config

For example

```
PORT=3334
SERVER_PORT=3333
```

Run local server

    $ npm start
   
### Production build
   
To build and minimize frontend files use command

    $ npm run build
    
This command puts all necessary files in directory `dist`

### Deploy frontend

Add deploy parameters to .env file

```
SSH_HOST=host.org
SSH_USERNAME=username
SSH_KEY_PATH=~/.ssh/id_rsa
SSH_PASSPHRASE=passphrase
UPLOAD_DIR=/var/www/cups
```

Start deploy

    $ gulp deploy

### Deploy backend

Install dependencies
    
    $ sudo apt-get update
    $ sudo apt-get install build-essential python-virtualenv python3-dev libpq-dev postgresql postgresql-contrib nginx git vim
   
Create database

    $ sudo su - postgres -c "psql"
    
    $ CREATE DATABASE cups_db;
    $ CREATE USER cups_db_user WITH PASSWORD '123';
    $ GRANT ALL PRIVILEGES ON DATABASE cups_db TO cups_db_user;
    $ \q
    
Add special user

    $ sudo adduser cups
    $ sudo su - cups
    
Generate deployment key

    $ ssh-keygen
    
Clone repository

    $ git clone git@bitbucket.org:vinneyto/cups.git && cd cups
    
Create python virtualenv

    $ virtualenv -p python3 venv && . venv/bin/activate
    $ cd backend && pip install -r requirements.txt

Create local config for django

    $ cp ../config/local_settings.py backend/local_settings.py && vim backend/local_settings.py
    $ python manage.py migrate
    $ python manage.py collectstatic
    $ python manage.py createsuperuser
    $ deactivate && exit
    
Create `systemctl` configuration

    $ sudo cp /home/cups/cups/config/cups_gunicorn.service /etc/systemd/system
    $ sudo systemctl start cups_gunicorn
    $ sudo systemctl enable cups_gunicorn
    
Create nginx configuration

    $ sudo cp /home/cups/cups/config/cups_nginx.conf /etc/nginx/sites-enabled
