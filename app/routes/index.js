import React from 'react';
import { Router } from 'react-router';
import { Provider } from 'mobx-react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

if (process.env.NODE_ENV === 'development') {
    require('../components/Layout');
    require('../components/LandingPage');
    require('../components/CupDetail');
    require('../components/Gallery');
}

const routes = {
    childRoutes: [
        {
            path: '/',
            getComponents(nextState, callback) {
                require.ensure([], function (require) {
                    callback(null, require('../components/Layout').default)
                })
            },
            getIndexRoute(partialNextState, callback) {
                require.ensure([], function (require) {
                    callback(null, {
                        component: require('../components/LandingPage').default,
                    })
                })
            },
            childRoutes: [
                {
                    path: '/cup/:uuid',
                    getComponents(nextState, callback) {
                        require.ensure([], function (require) {
                            callback(null, require('../components/CupDetail').default)
                        })
                    }
                },
                {
                    path: '/gallery',
                    getComponents(nextState, callback) {
                        require.ensure([], function (require) {
                            callback(null, require('../components/Gallery').default)
                        })
                    }
                },
            ]
        }
    ]
};


export default ({stores, history}) => {
    return (
        <MuiThemeProvider>
            <Provider {...stores}>
                <Router history={history} routes={routes}/>
            </Provider>
        </MuiThemeProvider>
    );
};
