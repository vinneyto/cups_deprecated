
const touchListenersMap = new Map();


export function getOffsetRect(elem) {
    // (1)
    let box = elem.getBoundingClientRect();

    // (2)
    let body = document.body;
    let docElem = document.documentElement;

    // (3)
    let scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    let scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    // (4)
    let clientTop = docElem.clientTop || body.clientTop || 0;
    let clientLeft = docElem.clientLeft || body.clientLeft || 0;

    // (5)
    let top  = box.top +  scrollTop - clientTop;
    let left = box.left + scrollLeft - clientLeft;

    return { top: Math.round(top), left: Math.round(left) }
}

export function touchInit(element) {

    function onTouch(event) {
        if (event.touches.length > 1 || (event.type == 'touchend' && event.touches.length > 0))
            return;

        let type = null;
        let touch = null;

        switch (event.type) {
            case 'touchstart':
                type = 'mousedown';
                touch = event.changedTouches[0];
                break;
            case 'touchmove':
                type = 'mousemove';
                touch = event.changedTouches[0];
                break;
            case 'touchend':
                type = 'mouseup';
                touch = event.changedTouches[0];
                break;
        }

        let newEvt = new MouseEvent(type, {
            bubbles: true,
            cancelable: true,
            view: window,
            detail: 0,
            screenX: touch.screenX,
            screenY: touch.screenY,
            clientX: touch.clientX,
            clientY: touch.clientY,
            ctrlKey: event.ctrlKey,
            shiftKey: event.shiftKey,
            altKey: event.altKey,
            metaKey: event.metaKey,
            button: 0,
            buttons: null,
            relatedTarget: event.target
        });

        event.target.dispatchEvent(newEvt);
    }
    
    element.addEventListener('touchstart', onTouch, false);
    element.addEventListener('touchend', onTouch, false);
    element.addEventListener('touchmove', onTouch, false);

    touchListenersMap.set(element, onTouch);
}

export function touchDestroy(element) {
    let onTouch = touchListenersMap.get(element);
    if (!onTouch) return;

    element.removeEventListener('touchstart', onTouch, false);
    element.removeEventListener('touchend', onTouch, false);
    element.removeEventListener('touchmove', onTouch, false);

    touchListenersMap.delete(onTouch);
}

export function debounce(func, wait, immediate) {
    let timeout;
    return function() {
        let context = this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}
