import * as THREE from 'three';


class Assets {

    constructor() {
        this._loadingManager = new THREE.LoadingManager();
        this._objLoader = new THREE.JSONLoader(this._loadingManager);
        this._imageLoader = new THREE.ImageLoader(this._loadingManager);
        this._cubeTextureLoader = new THREE.CubeTextureLoader(this._loadingManager);
        this._cubeTextureLoader.setPath('/assets/environment/');

        this.background = null;
        this.geometry = null;
        this.texture = null;
    }

    loadEnvironment() {
        if (!this._loadEnvironmentPromise) {
            let urls = ['pos-x.png', 'neg-x.png', 'pos-y.png', 'neg-y.png', 'pos-z.png', 'neg-z.png'];

            this._loadEnvironmentPromise = new Promise((resolve, reject) => {
                this._cubeTextureLoader.load(urls, (background) => {
                    this.background = background;
                    resolve(background);
                }, undefined, reject);
            });
        }

        return this._loadEnvironmentPromise;
    }

    loadModel() {
        if (!this._loadModelPromise) {
            this._loadModelPromise = new Promise((resolve, reject) => {
                this._objLoader.load('/assets/cup.js', (geometry) => {
                    this.geometry = geometry;
                    resolve(geometry);
                }, undefined, reject);
            });
        }
        return this._loadModelPromise;
    }

    loadTexture(path) {
        return new Promise((resolve, reject) => {
            this._imageLoader.load(path, (texture) => {
                this.texture = texture;
                resolve(texture);
            }, undefined, reject);
        })
    }
}

export default new Assets();
