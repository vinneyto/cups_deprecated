import { observable } from 'mobx';

function getPageSize() {
    let bodyWidth = document.body.offsetWidth;

    if (bodyWidth < 768) {
        return 'xs';
    } else if (bodyWidth < 992) {
        return 'sm';
    } else if (bodyWidth < 1200) {
        return 'md';
    } else {
        return 'lg';
    }
}

class AppState {
    @observable pageSize = null;

    constructor() {
        this.pageSize = getPageSize();

        window.addEventListener('resize', () => {
            this.pageSize = getPageSize();
        });
    }
}

export default new AppState();
