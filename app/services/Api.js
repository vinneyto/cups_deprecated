import axios from 'axios';

const http = axios.create({
    baseURL: '/api'
});

export class Api {
    fetchCupsOnLanding() {
        return http.get('/cup/landing')
            .then(response => response.data);
    }

    fetchCupByUuid(cupUuid) {
        return http.get(`/cup/uuid/${cupUuid}`)
            .then(response => response.data);
    }

    fetchImages(params) {
        return http.get('/gallery/public/', { params })
            .then(response => {
                this.galleryImageTotalCount = +response.headers['x-total-count'];
                return response.data;
            });
    }

    createOrder(formData) {
        return http.post('/order/public/', formData)
            .then(response => response.data);
    }
}


export default new Api();
