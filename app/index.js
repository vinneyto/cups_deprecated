import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { AppContainer } from 'react-hot-loader';
import appState from './services/AppState';
import Routes from './routes';

import './styles/main.scss';

injectTapEventPlugin();

const routingStore = new RouterStore();

const stores = {
    routing: routingStore,
    appState
};

const history = syncHistoryWithStore(browserHistory, routingStore);

const render = () => {
    ReactDOM.render(
        <AppContainer>
            <Routes stores={stores} history={history}/>
        </AppContainer>,
        document.getElementById('root')
    );
};

if (module.hot) {
    module.hot.accept('./routes', () => {
        render();
    });
}

render();

