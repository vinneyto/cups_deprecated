import { observable } from 'mobx';


class CupsStore {
    @observable cups = [];
}

export default new CupsStore();
