import validatorjs from 'validatorjs';
import MobxReactForm from 'mobx-react-form';

validatorjs.useLang('ru');


export default class BaseForm extends MobxReactForm {

    constructor(props) {
        const plugins = {dvr: validatorjs};
        super(props, { plugins });
    }

    bindings() {
        return {
            MaterialTextField: {
                id: 'id',
                name: 'name',
                type: 'type',
                value: 'value',
                label: 'floatingLabelText',
                placeholder: 'hintText',
                disabled: 'disabled',
                error: 'errorText',
                onChange: 'onChange',
                onBlur: 'onBlur',
                onFocus: 'onFocus',
                autoFocus: 'autoFocus',
            },
        };
    }
}
