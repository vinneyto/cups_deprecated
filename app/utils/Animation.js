
export class Animation {

    constructor(target, field, from, to, duration, fps = 60) {
        this.target = target;
        this.field = field;
        this.from = from;
        this.to = to;
        this.duration = Math.abs(duration || 0);

        this._dt = 1 / fps;
        this._nt = Math.floor(duration * fps);
        this._counter = 0;
    }

    update() {
        if (this._counter < this._nt) {
            this.target[this.field] = this.interpolate(this._counter * this._dt);
            if (++this._counter === this._nt) {
                this.target[this.field] = this.to;
                return true;
            }
            return false;
        }
        return true;
    }

    interpolate(t) {
        let k = (this.to - this.from) / this.duration;
        return this.from + k * t;
    }
}

export class EaseOutAnimation extends Animation{

    interpolate(t) {
        let c = this.to - this.from;
        t /= this.duration/2;
        if (t < 1) return c/2*t*t + this.from;
        t--;
        return -c/2 * (t*(t-2) - 1) + this.from;
    }
}
