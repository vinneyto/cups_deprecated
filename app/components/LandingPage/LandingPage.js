import React from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import PermPhoneMsg from 'material-ui/svg-icons/action/perm-phone-msg';
import {orange500} from 'material-ui/styles/colors';
import LandingPagePromo from '../LandingPagePromo';
import OrderForm from '../OrderForm';


import './style.scss';

export default class LandingPage extends React.Component {

    constructor() {
        super();

        this.state = {
            showOrderFormDescription: false
        };
    }

    render() {
        const { showOrderFormDescription } = this.state;

        return (
            <div className="landing-page">
                <LandingPagePromo className="landing-page__promo"/>

                <div className="landing-page__body lp-ad">
                    <div className="container">
                        <h3 className="lp-ad__header">
                            Отличные рисунки на кружках
                        </h3>

                        <div className="row">
                            <div className="col-md-4">
                                <div className="lp-ad__column">
                                    <div className="lp-ad__icon">
                                        <span>1</span>
                                    </div>
                                    <div className="lp-ad__text">
                                        <h3 className="mdl-typography--title" style={{marginTop: 0}}>Рисунок на заказ</h3>
                                        Сделаем вам рисунок на кружке по фотографии или по кофейной гуще.
                                        Угадаем ваши мысли, нарисуем ваши мечты. В офисе все очумеют.
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="lp-ad__column">
                                    <div className="lp-ad__icon">
                                        <span>2</span>
                                    </div>
                                    <div className="lp-ad__text">
                                        <h3 className="mdl-typography--title" style={{marginTop: 0}}>Предпросмотр</h3>
                                        Перед печатью для вас будет создан трехмерный макет кружки, дабы вы могли
                                        во всех подробностях рассмотреть наши художества.
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="lp-ad__column">
                                    <div className="lp-ad__icon">
                                        <span>3</span>
                                    </div>
                                    <div className="lp-ad__text">
                                        <h3 className="mdl-typography--title" style={{marginTop: 0}}>Доставка</h3>
                                        Передача готового товара (кружки) осуществляется строго в черте города.
                                        Заброшенные склады и порты отменяются.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container landing-page__body">
                    <h3 className="lp-ad__header">Заказать кружку</h3>
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <OrderForm
                                showDescription={showOrderFormDescription}
                                onRequestShowDescription={() => this.setState({showOrderFormDescription: true})}/>
                        </div>
                    </div>
                </div>

                <FloatingActionButton
                    className="landing-page__buy-btn"
                    zDepth={2}
                    backgroundColor={orange500}>
                    <PermPhoneMsg />
                </FloatingActionButton>
            </div>
        )
    }
}
