import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import Dropzone from 'react-dropzone';
import FlatButton from 'material-ui/FlatButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import {red400} from 'material-ui/styles/colors';

import './style.scss';


@observer
export default class MultipleAttachmentSelect extends Component {

    static ERROR_FILE_TYPE = 'ERROR_FILE_TYPE';
    static ERROR_FILE_SIZE = 'ERROR_FILE_SIZE';

    static propTypes = {
        type: PropTypes.string,
        model: PropTypes.object.isRequired
    };

    onDrop = (files) => {
        const { model } = this.props;

        model.attachments.push(...files.map(f => ({
            $file: f
        })));
        this.validate();
    };

    removeAttachment(index) {
        const { model } = this.props;

        if (index < model.attachments.length) {
            model.attachments.splice(index, 1);
        }
        this.validate();
    }

    validate() {
        const { type, model } = this.props;
        let error = null;

        if (type) {
            let typeList = type.split(',');
            for (let i = 0; i < model.attachments.length; i++) {
                let attachment = model.attachments[i];
                let filename = attachment.$file && attachment.$file.name;
                if (filename && !typeList.find(type => filename.split('.').pop() === type)) {
                    attachment.$error = true;
                    error = MultipleAttachmentSelect.ERROR_FILE_TYPE;
                }
            }
        }

        model.error = error;
    }

    getErrorMessage() {
        const { type, model } = this.props;

        switch (model.error) {
            case MultipleAttachmentSelect.ERROR_FILE_TYPE:
                return `Возможные тип файлов: ${type}`;
            case MultipleAttachmentSelect.ERROR_FILE_SIZE:
                return 'Превышен максимальный размер файлов';
        }
    }

    render() {
        const { model } = this.props;

        return (
            <div className="ma-select">
                {model.attachments.map((a, index) =>
                    <div key={index} className="ma-attachment">
                        <div className="ma-attachment__name" style={{color: a.$error ? red400 : 'inherit'}}>
                            {a.$file.name}
                        </div>
                        <div className="ma-attachment__remove">
                            <NavigationClose onTouchTap={() => this.removeAttachment(index)}/>
                        </div>
                    </div>)
                }

                <Dropzone className="ma-select__dropzone layout-column align-center" onDrop={this.onDrop}>
                    <FlatButton label="Выберите файлы"/>
                </Dropzone>

                {model.error &&
                    <div style={{textAlign: 'center', color: red400}}>
                        {this.getErrorMessage()}
                    </div>
                }
            </div>
        );
    }
}
