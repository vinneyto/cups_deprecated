import { observable } from 'mobx';


export default class MultipleAttachmentSelectModel {
    @observable attachments = [];
    @observable error = null;

    isValid() {
        return !this.error;
    }
}
