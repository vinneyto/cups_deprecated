import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import BaseForm from '../../utils/BaseForm';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import MultipleAttachmentSelect from '../MultipleAttachmentSelect';
import MultipleAttachmentSelectModel from '../MultipleAttachmentSelect/Model';
import { pink400 } from 'material-ui/styles/colors';
import LoadingOverlay from '../LoadingOverlay';
import Recaptcha from 'react-google-invisible-recaptcha';
import Api from '../../services/Api';


class OrderFormModel extends BaseForm {

    constructor() {
        const fields = [
            {
                name: 'fullname',
                label: 'Ваше имя *',
                rules: 'required',
                bindings: 'MaterialTextField'
            },
            {
                name: 'phone',
                label: 'Телефон *',
                rules: 'required',
                bindings: 'MaterialTextField'
            },
            {
                name: 'email',
                label: 'Email',
                rules: 'email',
                bindings: 'MaterialTextField'
            },
            {
                name: 'description',
                label: 'Описание',
                bindings: 'MaterialTextField'
            }
        ];
        super({fields});
    }
}


@observer
class FormField extends Component {

    render() {
        let { field, fullWidth, multiLine, rows } = this.props;
        return <TextField {...field.bind()} {...{fullWidth, multiLine, rows}}/>
    }
}


export default class OrderForm extends Component {

    static propTypes = {
        showDescription: PropTypes.bool,
        onRequestShowDescription: PropTypes.func
    };

    constructor() {
        super();

        this.state = {
            form: new OrderFormModel(),
            saving: false,
            success: false,
            error: false
        };
        this.attachmentsModel = new MultipleAttachmentSelectModel();
    }

    onSuccess = () => {
        if (!this.attachmentsModel.error) {
            this.recaptcha.execute();
        }
    };

    onError = () => {
        this.recaptcha.reset();
    };

    onResolved = () => {
        this.setState({saving: true});
        const { fullname, phone, email, description } = this.state.form.values();

        const data = new FormData();
        data.append('fullname', fullname);
        data.append('phone', phone);
        data.append('email', email);
        data.append('description', description);
        data.append('recaptcha_token', this.recaptcha.getResponse());

        this.attachmentsModel.attachments.forEach(a => {
            data.append('file', a.$file);
        });

        Api.createOrder(data)
            .then(() => this.setState({success: true, saving: false}))
            .catch(() => this.setState({error: true, saving: false}));
    };

    render() {
        const { showDescription, onRequestShowDescription } = this.props;
        const { form, saving, success, error } = this.state;

        if (success) {
            return (
                <h5 className="mdl-typography--text-center">
                    Заявка притяна! <br/><br/> Мы свяжемся с вами в ближайшее время
                </h5>
            )
        }

        if (error) {
            return (
                <div className="layout-column align-center">
                    <h5 className="mdl-typography--text-center">
                        Что-то пошло не так...
                    </h5>
                    <br/><br/>
                    <RaisedButton
                        label="Попробовать еще раз?"
                        secondary={true}
                        type="button"
                        onTouchTap={() => this.setState({error: false})}/>
                </div>
            )
        }

        return (
            <form onSubmit={e => form.onSubmit(e, {
                onSuccess: this.onSuccess,
                onError: this.onError
            })}>
                {saving && <LoadingOverlay/>}

                <Recaptcha
                    ref={ ref => this.recaptcha = ref }
                    sitekey="6LeY7SEUAAAAAB7WmFgoBjVTAEWRR-nNOMcaWInX"
                    onResolved={ this.onResolved }/>

                <FormField field={form.$('fullname')} fullWidth={true}/><br/>
                <FormField field={form.$('phone')} fullWidth={true}/><br/>
                <FormField field={form.$('email')} fullWidth={true}/><br/>

                {!showDescription &&
                    <div style={{
                        fontSize: '1rem',
                        fontWeight: 'bold',
                        color: pink400,
                        cursor: 'pointer',
                        marginTop: '1rem'
                    }}
                       onTouchTap={() => onRequestShowDescription && onRequestShowDescription()}>
                        Добавить описание и файлы
                    </div>
                }

                {showDescription &&
                    <div>
                        <FormField field={form.$('description')} fullWidth={true}
                                   multiLine={true}
                                   rows={4}/><br/>
                        <br/>
                        <MultipleAttachmentSelect type="jpg,jpeg,png" model={this.attachmentsModel}/>
                    </div>
                }

                <br/>
                <br/>
                <div className="layout-column align-center">
                    <RaisedButton label="Отправить" secondary={true} type="submit"/>
                </div>
            </form>
        );
    }
}
