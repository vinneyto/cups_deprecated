import React, { Component } from 'react';
import Api from '../../services/Api';
import CircularProgress from 'material-ui/CircularProgress';
import GalleryLayout from '../GalleryLayout';
import ScrollHandler from '../ScrollHandler';
import Lightbox from 'react-image-lightbox';

import './style.scss';


export default class Gallery extends Component {

    constructor() {
        super();

        this.currentPage = 0;
        this.itemsPerPage = 20;

        this.state = {
            loading: false,
            imageSets: [],
            allImages: [],
            loadedCount: 0,
            lightBoxIndex: 0,
            lightBoxIsOpen: false
        };
    }

    componentDidMount() {
        this.loadNextPage();
    }

    loadNextPage() {
        if (this.state.loading) return;
        this.setState({loading: true});

        (async () => {
            let payload = await Api.fetchImages({
                _limit: this.itemsPerPage,
                _offset: this.currentPage * this.itemsPerPage,
                _ordering: '-created'
            });

            let galleryImages = await Promise.all(payload.map(galleryImage => new Promise(((resolve, reject) => {
                let image = new Image();

                image.src = galleryImage.image_file.thumbnail;
                image.onload = () => resolve({
                    width: image.width,
                    height: image.height,
                    aspectRatio: image.width / image.height,
                    galleryImage
                });
                image.onerror = () => reject();
            }))));

            await new Promise(resolve => setTimeout(resolve, 500));

            this.setState({
                loading: false,
                imageSets: [...this.state.imageSets, galleryImages],
                loadedCount: this.state.loadedCount + galleryImages.length,
                allImages: this.state.allImages.concat(galleryImages)
            });

            this.currentPage++;
        })();
    }

    onScrollBottom = () => {
        this.loadNextPage();
    };

    getImageSrc(image) {
        return image.galleryImage.image_file.file;
    }

    render() {
        let { loading, imageSets, loadedCount, lightBoxIndex, lightBoxIsOpen, allImages } = this.state;

        return (
            <div className="gallery container">
                {imageSets.map((images, index) =>
                    <GalleryLayout key={index} items={images} renderItem={this.renderItem}/> )}

                {loading ?
                    <div className="layout-column align-center">
                        <CircularProgress size={50} thickness={7}/>
                    </div>
                    : null
                }
                {!loading && loadedCount < Api.galleryImageTotalCount ?
                    <ScrollHandler style={{height: 50}} onScrollBottom={this.onScrollBottom}/> : null
                }

                {lightBoxIsOpen &&
                    <Lightbox
                        mainSrc={this.getImageSrc(allImages[lightBoxIndex])}
                        nextSrc={this.getImageSrc(allImages[(lightBoxIndex + 1) % allImages.length])}
                        prevSrc={this.getImageSrc(allImages[(lightBoxIndex + allImages.length - 1) % allImages.length])}
                        imageCaption={allImages[lightBoxIndex].galleryImage.description || ''}

                        onCloseRequest={() => this.setState({ lightBoxIsOpen: false })}
                        onMovePrevRequest={() => this.setState({
                            lightBoxIndex: (lightBoxIndex + allImages.length - 1) % allImages.length,
                        })}
                        onMoveNextRequest={() => this.setState({
                            lightBoxIndex: (lightBoxIndex + 1) % allImages.length,
                        })}
                    />
                }
            </div>
        )
    }

    renderItem = (item) => {
        // let src = item.width > (width - 200) ?
        //     item.galleryImage.image_file.thumbnail : item.galleryImage.image_file.file; //dirty

        let { galleryImage } = item;
        let src = galleryImage.image_file.thumbnail;

        return (
            <div className="gallery-image">
                <div className="gallery-image-overlay" onTouchTap={() => this.setState({
                    lightBoxIsOpen: true,
                    lightBoxIndex: this.state.allImages.indexOf(item)
                })}>
                    {galleryImage.description ?
                        <div className="gallery-image-overlay__text">
                            {galleryImage.description}
                        </div> : null
                    }
                </div>

                <img src={src} style={{border: 'none', width: '100%', height: '100%'}}/>
            </div>
        );
    };
}
