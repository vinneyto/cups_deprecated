import React, {Component} from 'react';
import CupViewer from '../CupViewer';
import Api from '../../services/Api';
import Assets from '../../services/Assets';
import CircularProgress from 'material-ui/CircularProgress';

import './style.scss';

/**
 * Компонент для отображения увеличенного изображения кружки
 */
export default class CupDetail extends Component {

    constructor() {
        super();

        this.state = {
            cup: null,
            background: null,
            texture: null,
            loading: true,
            geometry: null
        };
    }

    componentDidMount() {
        let state = {};

        Promise.all([
            Assets.loadEnvironment(),
            Assets.loadModel()
        ])
            .then(() => Api.fetchCupByUuid(this.props.params.uuid))
            .then((cup => {
                state.cup = cup;
                return Assets.loadTexture(cup.texture.file);
            }))
            .then((texture) => {
                state.texture = texture;
                state.background = Assets.background;
                state.geometry = Assets.geometry;
                state.loading = false;

                this.setState(state);
            });
    }

    render() {
        let { background, texture, geometry, loading } = this.state;

        return (
            <div className="cup-detail">
                {background && texture && geometry ?
                    <CupViewer className="cup-detail__view"
                               background={background}
                               texture={texture}
                               geometry={geometry}/>
                    : null
                }

                {loading ?
                    <div className="cup-detail__loader">
                        <CircularProgress size={100} thickness={7}/>
                    </div> : null
                }
            </div>
        )
    }
}
