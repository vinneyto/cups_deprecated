import React from 'react';
import './style.scss';
import HeaderBar from '../HeaderBar';


export default class Layout extends React.Component {

    render() {
        return (
            <div className="layout">
                <HeaderBar className="layout__header"/>
                <div className="layout__body">
                    {this.props.children}
                </div>
            </div>
        )
    }
}
