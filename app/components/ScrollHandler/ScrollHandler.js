import React, {Component, PropTypes} from 'react';


export default class ScrollHandler extends Component {

    static propTypes = {
        onScrollBottom: PropTypes.func
    };

    componentDidMount() {
        document.addEventListener('scroll', this.onDocumentScroll);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.onDocumentScroll);
    }

    onDocumentScroll = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && this.props.onScrollBottom) {
            this.props.onScrollBottom();
        }
    };

    render() {
        let props = {
            style: this.props.style,
            className: this.props.className
        };
        return <div {...props}/>;
    }
}
