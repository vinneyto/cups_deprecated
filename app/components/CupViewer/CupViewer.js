import React, { Component, PropTypes } from 'react';
import * as THREE from 'three';
import * as Utils from '../../services/Utils';

import './style.scss';

const ROTATION_VELOCITY = 0.1;
const INITIAL_ROTATION = Math.PI * 0.8;

export default class CupViewer extends Component {

    static propTypes = {
        className: PropTypes.string,
        background: PropTypes.object.isRequired,
        geometry: PropTypes.object.isRequired,
        texture: PropTypes.object.isRequired
    };

    componentDidMount() {
        if (!window.WebGLRenderingContext) {
            return;
        }

        this.rotationAngle = INITIAL_ROTATION;
        this.rotationRadius = 6;
        this.rotationVelocity = 0;
        this.rotationRequiredVelocity = ROTATION_VELOCITY;
        this.mouseX = 0;

        this.dragging = false;

        this.containerWidth = this.container.offsetWidth;
        this.containerHeight = this.container.offsetHeight;

        this.clock = new THREE.Clock();

        this.camera = new THREE.PerspectiveCamera(45, this.containerWidth / this.containerHeight, 1, 2000);
        this.camera.position.z = this.rotationRadius;
        this.camera.position.y = 2;

        this.scene = new THREE.Scene();

        // let axisHelper = new THREE.AxisHelper( 5 );
        // this.scene.add(axisHelper);

        let ambient = new THREE.AmbientLight(0x999999);
        this.scene.add(ambient);

        let directionalLight = new THREE.DirectionalLight(0xFFD7B3);
        directionalLight.position.set(-1, 0, 1);
        directionalLight.intensity = 0.4;
        this.scene.add(directionalLight);

        directionalLight = new THREE.DirectionalLight(0xffffff);
        directionalLight.position.set(0, 1, -1);
        directionalLight.intensity = 0.3;
        this.scene.add(directionalLight);

        this.cupTexture = new THREE.Texture();

        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize(this.containerWidth, this.containerHeight);

        let animate = () => {
            this.reqId = requestAnimationFrame(animate);
            this.renderScene();
        };

        this.scene.background = this.props.background;

        this.cupMesh = new THREE.Mesh(this.props.geometry, new THREE.MeshLambertMaterial());
        this.cupMesh.position.y = -1.5;
        this.cupMesh.rotation.y = Math.PI;
        this.cupMesh.material.map = this.cupTexture;
        this.cupMesh.material.envMap = this.scene.background;
        this.cupMesh.material.reflectivity = 0.1;
        this.scene.add(this.cupMesh);

        this.cupTexture.image = this.props.texture;
        this.cupTexture.needsUpdate = true;

        Utils.touchInit(document);

        document.addEventListener('mousedown', this.onDocumentMouseDown, false);
        window.addEventListener('resize', this.onWindowResize, false);

        animate();

        this.container.appendChild(this.renderer.domElement);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.texture !== this.props.texture) {
            this.cupTexture.image = this.props.texture;
            this.cupTexture.needsUpdate = true;
        }
    }

    componentWillUnmount() {
        if (this.reqId) {
            cancelAnimationFrame(this.reqId);
            this.reqId = null;
        }

        document.removeEventListener('mousedown', this.onDocumentMouseDown, false);
        window.removeEventListener( 'resize', this.onWindowResize, false );

        Utils.touchDestroy(document);
    }

    onDocumentMouseDown = (event) => {
        let offset = Utils.getOffsetRect(this.container);
        let pageX = event.pageX;
        let pageY = event.pageY;

        if (pageX >= offset.left &&
            pageX <= offset.left + this.container.offsetWidth &&
            pageY >= offset.top &&
            pageY <= offset.top + this.container.offsetHeight
        ) {
            this.dragging = true;
            this.mouseX = event.pageX;
            this.rotationRequiredVelocity = 0;

            document.addEventListener('mousemove', this.onDocumentMouseMove, false);
            document.addEventListener('mouseup', this.onDocumentMouseUp, false);
        }
    };

    onDocumentMouseMove = (event) => {
        this.rotationRequiredVelocity = (event.pageX - this.mouseX) * 0.7;

        this.mouseX = event.pageX;
    };

    onDocumentMouseUp = () => {
        this.dragging = false;
        this.rotationRequiredVelocity = ROTATION_VELOCITY;

        document.removeEventListener('mousemove', this.onDocumentMouseMove, false);
        document.removeEventListener('mouseup', this.onDocumentMouseUp, false);
    };

    onWindowResize = () => {
        this.containerWidth = this.container.offsetWidth;
        this.containerHeight = this.container.offsetHeight;

        this.camera.aspect = this.containerWidth / this.containerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.containerWidth , this.containerHeight);
    };

    onContainerRef = (element) => {
        this.container = element;
    };

    renderScene() {
        let delta = this.clock.getDelta();

        delta = Math.min(delta, 0.04);

        if (this.dragging) {
            this.rotationRequiredVelocity -= this.rotationRequiredVelocity * delta * 3;
        }

        this.rotationVelocity += (this.rotationRequiredVelocity - this.rotationVelocity) * delta * 2;
        this.rotationAngle += delta * this.rotationVelocity;

        this.camera.position.x = this.rotationRadius * Math.cos(this.rotationAngle);
        this.camera.position.z = this.rotationRadius * Math.sin(this.rotationAngle);

        this.camera.lookAt(this.scene.position);
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return (
            <div className={'cups-viewer ' + (this.props.className || '')}>
                <div className="cups-viewer__scene cups-viewer__scene--appear"
                     ref={this.onContainerRef}></div>
            </div>
        )
    }
}
