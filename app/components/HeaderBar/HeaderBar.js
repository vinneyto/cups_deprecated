import React, { Component } from 'react';
import {inject, observer} from 'mobx-react';
import { Link, browserHistory } from 'react-router';
import Paper from 'material-ui/Paper';
import {pink400} from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import IconVk from '../../icons/IconVk';
// import IconFb from '../../icons/IconFb';
// import IconGp from '../../icons/IconGp';
import IconIn from '../../icons/IconIn';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import OrderFormModal from '../OrderFormModal';

import './style.scss';

@inject('appState')
@observer
export default class HeaderBar extends Component {

    constructor() {
        super();
        this.state = {
            menuOpen: false,
            showOrderModal: false
        }
    }

    goToPage(url) {
        browserHistory.push(url);
        this.setState({ menuOpen: false });
    }

    render() {
        let { appState } = this.props;
        let { showOrderModal } = this.state;

        return (
            <Paper
                className={'header-bar ' + this.props.className}
                zDepth={1}
                rounded={false}>

                {appState.pageSize === 'xs' ?
                    <IconButton onTouchTap={() => this.setState({menuOpen: true})}>
                        <MenuIcon color={pink400}/>
                    </IconButton>
                    :
                    null
                }

                {appState.pageSize === 'xs' ?
                    <Drawer
                        docked={false}
                        width={200}
                        open={this.state.menuOpen}
                        onRequestChange={(menuOpen) => this.setState({menuOpen})}
                    >
                        <Paper className="header-bar" rounded={false} style={{height: 60}}>
                            <Link to="/" onTouchTap={() => this.setState({menuOpen: false})}
                                  className="header-bar__brand" style={{marginLeft: '1rem'}}>
                                <span className="header-bar__brand-christmas"/>
                                MiuCup
                            </Link>
                        </Paper>
                        <MenuItem onTouchTap={() => this.goToPage('/gallery')}>Галерея</MenuItem>
                        <MenuItem>Цены</MenuItem>
                        <MenuItem>О нас</MenuItem>
                    </Drawer>
                    :
                    null
                }

                <Link to="/" className="header-bar__brand">
                    <span className="header-bar__brand-christmas"/>
                    MiuCup
                </Link>

                {appState.pageSize !== 'xs' ?
                    <div className="header-bar-menu">
                        <Link to="/gallery" activeClassName="active" className="header-bar-menu__item">
                            Галерея
                        </Link>
                        <a href="#" className="header-bar-menu__item">
                            Цены
                        </a>
                        <a href="#" className="header-bar-menu__item">
                            О нас
                        </a>
                    </div>
                    :
                    null
                }
                
                <div className="header-bar-social" style={{marginLeft: 'auto'}}>
                    <a href="https://vk.com/miucup" target="_blank" className="header-bar-social__item">
                        <IconVk/>
                    </a>
                    {/*<a href="#" className="header-bar-social__item">*/}
                        {/*<IconFb/>*/}
                    {/*</a>*/}
                    <a href="https://www.instagram.com/miucup/" target="_blank" className="header-bar-social__item">
                        <IconIn/>
                    </a>
                    {/*<a href="#" className="header-bar-social__item">*/}
                        {/*<IconGp/>*/}
                    {/*</a>*/}
                </div>

                {appState.pageSize !== 'xs' &&
                    <RaisedButton
                        label="Хотите кружку ?"
                        secondary={true}
                        style={{marginRight: '1rem'}}
                        onTouchTap={() => this.setState({showOrderModal: true})}/>
                }

                <OrderFormModal
                    open={showOrderModal}
                    onRequestClose={() => this.setState({showOrderModal: false})}/>
            </Paper>
        );
    }
}
