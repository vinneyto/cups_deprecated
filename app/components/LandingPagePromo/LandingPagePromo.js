import React from 'react';
import { Link } from 'react-router';
import CupViewer from '../CupViewer';
import Api from '../../services/Api';
import Assets from '../../services/Assets';
import CircularProgress from 'material-ui/CircularProgress';
import ChevronLeftIcon from 'material-ui/svg-icons/navigation/chevron-left';
import ChevronRightIcon from 'material-ui/svg-icons/navigation/chevron-right';
import IconLaunch from 'material-ui/svg-icons/action/launch';
import OrderFormModal from '../OrderFormModal';

import './style.scss';


export default class LandingPagePromo extends React.Component {

    static propTypes = {
        onCupChange: React.PropTypes.func
    };

    constructor() {
        super();

        this.cupsOnLanding = [];

        this.state = {
            selectedCup: null,
            background: null,
            texture: null,
            loading: false,
            geometry: null,
            showOrderModal: false
        };
    }

    componentDidMount() {
        (async () => {
            try {
                let [
                    cupsOnLanding,
                    background,
                    geometry
                ] = await Promise.all([
                    Api.fetchCupsOnLanding(),
                    Assets.loadEnvironment(),
                    Assets.loadModel()
                ]);

                this.cupsOnLanding = cupsOnLanding.concat();
                this.cupsOnLanding.sort((a, b) => a.priority - b.priority);

                this.setState({background, geometry, loading: true});

                // Для более плавной анимации меню
                await new Promise(resolve => setTimeout(resolve, 500));

                await this.switchCup(true);
            } catch (e) {
                console.log('error loading', e);
            }
        })();
    }

    switchCup(positive) {
        let cupsOnLanding = this.cupsOnLanding;
        if (!cupsOnLanding.length) {
            this.setState({loading: false, selectedCup: null});
            return Promise.resolve();
        }

        let currentIndex = this.state.selectedCup ?
            cupsOnLanding.findIndex(c => c.id === this.state.selectedCup.id) : -1;

        let selectedCupIndex = currentIndex + (positive ? 1 : -1);
        if (selectedCupIndex< 0) {
            selectedCupIndex = cupsOnLanding.length - 1;
        }
        if (selectedCupIndex >= cupsOnLanding.length) {
            selectedCupIndex = 0;
        }

        let selectedCup = cupsOnLanding[selectedCupIndex];
        let loadTexturePromise = selectedCup.$texture ?
            Promise.resolve(selectedCup.$texture) : Assets.loadTexture(selectedCup.texture.file);

        let loaded = false;

        setTimeout(() => {
            if (!loaded) { this.setState({loading: true}); }
        }, 1000);

        this.setState({selectedCup});

        if (this.props.onCupChange) {
            this.props.onCupChange(selectedCup);
        }

        return loadTexturePromise.then((texture) => {
            selectedCup.$texture = texture;
            if (this.state.selectedCup.id === selectedCup.id) {
                this.setState({texture: selectedCup.$texture, loading: false});
            }
            loaded = true;
            return selectedCup;
        });
    }

    render() {
        let {
            background,
            texture,
            geometry,
            loading,
            selectedCup,
            showOrderModal
        } = this.state;

        return (
            <div className={'lp-promo ' + this.props.className}>
                {background && texture && geometry ?
                    <CupViewer className="lp-promo__cup-view"
                               background={background}
                               texture={texture}
                               geometry={geometry}/>
                    : null
                }

                <div className="lp-promo__controls">
                    <div className="container">
                        <div className="row">
                            <div className="lp-promo__info col-md-4 col-sm-4 col-lg-3">
                                <div className="lp-promo__text">
                                    Авторские рисунки на кружках
                                </div>
                                <div className="lp-promo__button"
                                     onTouchTap={() => this.setState({showOrderModal: true})}>Заказать</div>

                                <OrderFormModal
                                    open={showOrderModal}
                                    onRequestClose={() => this.setState({showOrderModal: false})}/>
                            </div>
                        </div>
                    </div>

                    <button className="lp-promo__chevron lp-promo__chevron--left"
                            onTouchTap={() => this.switchCup(false)}>
                        <ChevronLeftIcon color="white"/>
                    </button>

                    <button className="lp-promo__chevron lp-promo__chevron--right"
                            onTouchTap={() => this.switchCup(true)}>
                        <ChevronRightIcon color="white"/>
                    </button>

                    {selectedCup ?
                        <Link to={`/cup/${selectedCup.uuid}`} target="_blank" className="lp-promo__cup-detail-btn">
                            <IconLaunch color="white"/>
                        </Link> : null
                    }

                    {loading ?
                        <div className="lp-promo__loader">
                            <CircularProgress size={50} thickness={7}/>
                        </div>
                        : null
                    }
                </div>
            </div>
        );
    }
}
