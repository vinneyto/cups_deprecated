import React, { Component, PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import OrderForm from '../OrderForm';
import NavigationClose from 'material-ui/svg-icons/navigation/close';


export default class OrderFormModal extends Component {

    static propTypes = {
        open: PropTypes.bool,
        onRequestClose: PropTypes.func.isRequired
    };

    constructor() {
        super();

        this.state = {
            showOrderFormDescription: false
        };
    }

    onRequestClose = () => {
        this.setState({showOrderFormDescription: false});
        this.props.onRequestClose();
    };

    render() {
        const { open } = this.props;
        const { showOrderFormDescription } = this.state;

        return (
            <Dialog open={open}
                    onRequestClose={() => this.onRequestClose()}
                    autoScrollBodyContent={true}
                    repositionOnUpdate={false}
                    style={{paddingTop: 0}}>

                <div className="layout-row">
                    <div style={{marginRight: 'auto'}}/>
                    <NavigationClose
                        style={{cursor: 'pointer', fill: 'aaa'}}
                        onTouchTap={() => this.onRequestClose()}/>
                </div>

                <OrderForm
                    showDescription={showOrderFormDescription}
                    onRequestShowDescription={() => this.setState({showOrderFormDescription: true})}/>
            </Dialog>
        );
    }
}
