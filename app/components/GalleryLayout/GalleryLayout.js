import React, { Component, PropTypes } from 'react';
import {debounce} from '../../services/Utils';

import './style.scss';

export default class GalleryLayout extends Component {

    static propTypes = {
        renderItem: PropTypes.func.isRequired,
        items: PropTypes.array.isRequired,
        itemWidth: PropTypes.number,
        gap: PropTypes.number
    };

    constructor() {
        super();

        this.defaultItemWidth = 200;
        this.defaultGap = 4;

        this.state = {
            containerWidth: 0
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (this.props.items !== nextProps.items ||
                this.props.itemWidth !== nextProps.itemWidth ||
                this.state.containerWidth !== nextState.containerWidth);
    }

    componentDidMount() {
        window.addEventListener('resize', this.onUpdateContainerWidthDebounced);
        this.onUpdateContainerWidth();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onUpdateContainerWidthDebounced);
    }

    onUpdateContainerWidth = () => {
        this.setState({containerWidth: this.container.offsetWidth});
    };

    onUpdateContainerWidthDebounced = debounce(this.onUpdateContainerWidth, 50);

    setContainerRef = (element) => {
        this.container = element;
    };

    render() {
        let result = this.state.containerWidth > 0 ? this.renderItems() : null;

        return (
            <div ref={this.setContainerRef}
                 className="gallery-layout"
                 style={{height: result && result.height}}
            >
                {result && result.elements}
            </div>
        )
    }

    renderItems() {
        const itemWidth = this.props.itemWidth || this.defaultItemWidth;
        const containerWidth = this.state.containerWidth;
        const maxAspectRatio = Math.max(Math.floor(containerWidth / itemWidth), 1);
        const gap = this.props.gap || this.defaultGap;
        const items = this.props.items;

        let elements = [];
        let aspectRatio = 0;
        let rowStartIndex = 0;
        let top = 0;

        for (let i = 0; i < items.length; i++) {
            let item = items[i];
            let nextItem = (i < items.length - 1) ? items[i + 1] : null;

            aspectRatio += item.aspectRatio;

            if (!nextItem || (aspectRatio + nextItem.aspectRatio > maxAspectRatio)) {
                let count = i - rowStartIndex + 1;
                let commonHeight = (containerWidth - gap * (count - 1)) / aspectRatio;
                let left = 0;

                for (let j = rowStartIndex; j <= i; j++) {
                    item = items[j];
                    let width = item.aspectRatio * commonHeight;
                    elements.push(this.renderItemContainer(j, width, commonHeight, left, top, item));
                    left += (width + gap);
                }
                top += (commonHeight + gap);
                rowStartIndex = i + 1;
                aspectRatio = 0;
            }
        }

        return { elements, height: top };
    }

    renderItemContainer(index, width, height, left, top, item) {
        return (
            <div key={index}
                 className="gallery-layout__item"
                 style={{width, height, left, top}}
            >
                {this.props.renderItem(item, width, height)}
            </div>
        );
    }
}
