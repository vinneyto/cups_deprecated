import React, { Component }  from 'react';
import CircularProgress from 'material-ui/CircularProgress';

import './style.scss';

export default class LoadingOverlay extends Component {

    render() {
        return (
            <div className={'loading-overlay ' + (this.props.className || '')} style={this.props.style}>
                <CircularProgress size={60} thickness={7} />
            </div>
        )
    }
}
