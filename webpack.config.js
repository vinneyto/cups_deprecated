const path = require('path');
const webpack = require('webpack');
const CleanPlugin = require('clean-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
require('dotenv').config();


module.exports = (env = {}) => {

    let PROD = env.production;

    function addHash(template, hash) {
        return PROD ? template.replace(/\.[^.]+$/, `.[${hash}]$&`) : `${template}`;
    }

    return {
        context: path.resolve(__dirname, './app'),
        entry: [
            'react-hot-loader/patch',
            'babel-polyfill',
            './index.js'
        ],
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: addHash('scripts.js', 'chunkhash')
        },

        devtool: !PROD ? 'cheap-inline-module-source-map' : false,

        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: 'eslint-loader',
                },
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: 'babel-loader',
                },
                {
                    test: /\.json$/,
                    exclude: /node_modules/,
                    use: 'json-loader',
                },
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            'css-loader?importLoaders=1',
                            'postcss-loader',
                            'sass-loader'
                        ]
                    })
                },
                {
                    test: /\.(eot|ttf|woff|woff2|svg|svgz)(\?.*)?$/,
                    use: 'file-loader?name=fonts/[name].[ext]'
                },
                {
                    test: /\.(png|jpg|jpeg)/,
                    use: addHash('file-loader?name=[name].[ext]', 'hash')
                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                template: 'index.html',
                inject: 'body'
            }),
            new CleanPlugin(['dist']),
            new HtmlPlugin({
                template: 'index.html',
                inject: 'body'
            }),
            new CopyPlugin([
                {from: 'favicon.ico'},
                {from: 'favicon-16x16.png'},
                {from: 'favicon-32x32.png'},
                {from: 'favicon-96x96.png'},
                {from: 'assets/cup.js', to: 'assets'},
                {from: 'assets/lena2.png', to: 'assets'},
                {from: 'assets/environment', to: 'assets/environment'}
            ]),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: PROD ? '"production"' : '"development"'
                },
                __DEVELOPMENT__: !PROD
            }),
            new ExtractTextPlugin(addHash('styles.css', 'hash')),
            PROD ? new CompressionPlugin({
                asset: '[path].gz[query]',
                algorithm: 'gzip',
                test: /\.(js|css)$/,
                threshold: 10240,
                minRatio: 0.8
            }) : null
        ].filter(p => p),

        devServer: {
            port: process.env.PORT,
            contentBase: path.resolve(__dirname, './app'),
            historyApiFallback: true,
            proxy: [
                {
                    context: ['/api/**', '/media/**'],
                    target: `http://localhost:${process.env.SERVER_PORT}`,
                }
            ]
        }
    }
};
