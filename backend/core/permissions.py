
MODEL_OPERATIONS = ['add', 'change', 'delete']


def check_permission(*params):
    """
    APIView request method decorator
    
    :param params: 
    :return: 
    """
    def _check_permission(f):
        def wrapper(self, *args, **kwargs):
            request = args[0]
            perms = []
            for param in params:
                model, operation = param
                assert operation in MODEL_OPERATIONS, (
                    'permissions type can be only in: {}'.format(MODEL_OPERATIONS)
                )
                perms.append('{}.{}_{}'.format(
                    model._meta.app_label,
                    operation,
                    model._meta.model_name,
                ))

            if not request.user.has_perms(perms):
                self.permission_denied(request)

            return f(self, *args, **kwargs)
        return wrapper
    return _check_permission
