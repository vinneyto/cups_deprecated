from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^attachment/$', views.AttachmentCreateView.as_view()),
]
