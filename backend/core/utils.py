from rest_framework import serializers
from django.conf import settings
import requests


class UserSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    email = serializers.EmailField()
    username = serializers.CharField(max_length=100)


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data
    }


def check_recaptcha_token(token):
    data = requests.post(settings.RECAPTCHA_URL, data={
        'secret': settings.RECAPTCHA_KEY,
        'response': token
    }).json()
    return 'success' in data and data['success']
