from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response


class LimitOffsetHeaderPagination(LimitOffsetPagination):
    limit_query_param = '_limit'
    offset_query_param = '_offset'

    def get_paginated_response(self, data):
        return Response(data, headers={
            'X-Total-Count': self.count
        })
