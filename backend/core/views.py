from rest_framework.exceptions import APIException
from rest_framework.generics import ListCreateAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import Attachment
from core.permissions import check_permission
from core.serializers import AttachmentSerializer


class ListCreateDetailsAPIView(ListCreateAPIView):
    list_serializer = None
    detail_serializer = None

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return self.detail_serializer
        return self.list_serializer


class AttachmentCreateView(APIView):
    parser_classes = (MultiPartParser,)
    permission_classes = (IsAuthenticated, IsAdminUser, )

    @check_permission((Attachment, 'add'))
    def post(self, request):
        file = request.data['file']

        attachment = Attachment(filename=file.name, file=file)
        try:
            attachment.save()
        except Exception as e:
            raise APIException(detail=str(e))

        return Response(AttachmentSerializer(attachment).data)
