import uuid
import os
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver


def upload_url(obj, filename):
    ext = filename.split('.')[-1]
    return os.path.join('upload/attachments/', '{}.{}'.format(uuid.uuid4().hex, ext))


class Attachment(models.Model):
    filename = models.CharField(max_length=128)
    file = models.FileField(upload_to=upload_url, verbose_name="Файл")

    def __str__(self):
        return self.filename

    class Meta:
        verbose_name = 'Вложение'
        verbose_name_plural = 'Вложения'


@receiver(pre_delete, sender=Attachment)
def attachment_pre_delete(sender, instance, **kwargs):
    instance.file.delete(False)
