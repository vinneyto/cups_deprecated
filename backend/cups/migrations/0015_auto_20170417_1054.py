# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-17 10:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cups', '0014_auto_20170417_1012'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cup',
            name='texture',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='cups.CupTexture', verbose_name='Текстуры'),
        ),
    ]
