from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions, IsAdminUser
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework.response import Response
from django.http import Http404

from core.permissions import check_permission
from cups.serializers import CupSerializer, CupTextureSerializer
from cups.models import Cup, CupTexture


# Public views

class CupOnLandingList(ListAPIView):
    queryset = Cup.objects.filter(show_on_landing=True).select_related('texture')
    serializer_class = CupSerializer


class CupByUuidDetail(APIView):
    def get_object(self, cup_uuid):
        try:
            return Cup.objects.get(uuid=cup_uuid)
        except Cup.DoesNotExist:
            raise Http404

    def get(self, request, cup_uuid):
        cup = self.get_object(cup_uuid)
        serialized = CupSerializer(cup)
        return Response(serialized.data)


# Admin views

class CupListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser, DjangoModelPermissions, )
    queryset = Cup.objects.all().select_related('texture')
    serializer_class = CupSerializer
    filter_fields = {
        'name': ['exact', 'icontains'],
        'show_on_landing': ['exact']
    }
    ordering_fields = ('name', 'show_on_landing', 'priority', )


class CupRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser, DjangoModelPermissions, )
    queryset = Cup.objects.all().select_related('texture')
    serializer_class = CupSerializer


class CupSetPriorityView(APIView):
    """
    Выставляет для указанной кружки приоритет
    Смещает остальные кружки вверх или вниз а зависимости от
    направления перемещения указанной кружки
    """
    permission_classes = (IsAuthenticated, IsAdminUser, )

    @check_permission((Cup, 'change'))
    def put(self, request, pk, priority):
        try:
            cup = Cup.objects.get(pk=pk)
        except Cup.DoesNotExist:
            raise Http404()

        cup.set_priority(int(priority))

        return Response(CupSerializer(cup).data)


class CupAddOnLandingView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser, )

    @check_permission((Cup, 'change'))
    def put(self, request, pk, position):
        try:
            cup = Cup.objects.get(pk=pk)
        except Cup.DoesNotExist:
            raise Http404()

        to_beginning = True if position == 'start' else False

        cup.add_on_landing(to_beginning)

        return Response(CupSerializer(cup).data)


class CupRemoveFromLanding(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser, )

    @check_permission((Cup, 'delete'))
    def delete(self, request, pk):
        try:
            cup = Cup.objects.get(pk=pk)
        except Cup.DoesNotExist:
            raise Http404()

        cup.remove_from_landing()

        return Response(CupSerializer(cup).data)


class CupTextureCreateView(APIView):
    parser_classes = (MultiPartParser,)
    permission_classes = (IsAuthenticated, IsAdminUser, )

    @check_permission((CupTexture, 'add'))
    def post(self, request):
        file = request.data['file']

        cup_texture = CupTexture(filename=file.name, file=file)
        cup_texture.save()

        return Response(CupTextureSerializer(cup_texture).data)
