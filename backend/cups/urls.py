from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.CupListCreateView.as_view()),
    url(r'^(?P<pk>\d+)/$', views.CupRetrieveUpdateDestroyView.as_view()),
    url(r'^(?P<pk>\d+)/priority/(?P<priority>\d+)/$', views.CupSetPriorityView.as_view()),

    url(r'^(?P<pk>\d+)/landing/$', views.CupRemoveFromLanding.as_view()),
    url(r'^(?P<pk>\d+)/landing/(?P<position>.+)/$', views.CupAddOnLandingView.as_view()),

    url(r'^texture/$', views.CupTextureCreateView.as_view()),

    url(r'^landing/$', views.CupOnLandingList.as_view()),
    url(r'^uuid/(?P<cup_uuid>.+)/$', views.CupByUuidDetail.as_view()),
]
