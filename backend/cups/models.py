import os
import uuid
from django.db import models, transaction
from django.db.models import Max
from django.db.models.signals import post_delete, pre_delete
from django.dispatch import receiver

from cups.exceptions import CupNotOnLandingException, CupAlreadyOnLandingException


def upload_url(obj, filename):
    ext = filename.split('.')[-1]
    return os.path.join('upload/cups/', '{}.{}'.format(uuid.uuid4().hex, ext))


class CupTexture(models.Model):
    """
    Текстура кружки. Задается как отдельная сущность для удобства
    Управления текстурами с помощью REST
    """
    filename = models.CharField(max_length=128)
    file = models.ImageField(upload_to=upload_url, verbose_name="Файл с текстурой")

    def __str__(self):
        return self.filename

    class Meta:
        verbose_name = 'Текстура'
        verbose_name_plural = 'текстуры'


class Cup(models.Model):
    """
    Модель для работы с кружками. содержит текстуру CupTexture
    """
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, verbose_name="Имя")
    texture = models.OneToOneField(CupTexture, verbose_name='Текстура')
    show_on_landing = models.BooleanField(
        default=False, verbose_name="Показывать на главной странице"
    )
    priority = models.IntegerField(default=0, verbose_name="Приоритет")

    def __str__(self):
        return self.name

    @transaction.atomic
    def set_priority(self, priority):
        """
        Устанавливает приоритет кружки, смещает приоритет остальных
        :param priority: 
        :return: 
        """
        if not self.show_on_landing:
            raise CupNotOnLandingException(self)

        if self.priority == priority:
            return

        if priority < 0:
            priority = 0

        # приоритет увеличивается, у остальных уменьшается
        index = -1 if priority > self.priority else 1
        from_priority = priority if index > 0 else self.priority
        to_priority = priority if index < 0 else self.priority

        cups_on_landing = Cup.objects.filter(
            show_on_landing=True,
            priority__gte=from_priority,
            priority__lte=to_priority
        )

        for other_cup in cups_on_landing:
            if other_cup != self:
                other_cup.priority += index
                other_cup.save()

        self.priority = priority
        self.save()

    @transaction.atomic
    def add_on_landing(self, to_beginning):
        """
        Добавляет кружку на главную страницу. Смещает остальные кружки
        :param to_beginning: 
        :return: 
        """
        if self.show_on_landing:
            raise CupAlreadyOnLandingException(self)

        self.show_on_landing = True

        if not to_beginning:
            max_priority_on_landing = Cup.objects.filter(show_on_landing=True)\
                .aggregate(Max('priority'))['priority__max']
            if max_priority_on_landing is not None:
                max_priority_on_landing += 1
            else:
                max_priority_on_landing = 0
            self.priority = max_priority_on_landing
        else:
            for cup in Cup.objects.filter(show_on_landing=True):
                cup.priority += 1
                cup.save()
            self.priority = 0

        self.save()

    @transaction.atomic
    def remove_from_landing(self):
        """
        Удаляет кружку с главной. Смещает назад все последующие кружки
        :return: 
        """
        if not self.show_on_landing:
            raise CupNotOnLandingException(self)

        for cup in Cup.objects.filter(show_on_landing=True, priority__gt=self.priority):
            cup.priority -= 1
            cup.save()

        self.priority = 0
        self.show_on_landing = False
        self.save()

    class Meta:
        verbose_name = 'Чашка'
        verbose_name_plural = 'Чашки'


@receiver(pre_delete, sender=Cup)
def cup_pre_delete(sender, instance, **kwargs):
    if instance.show_on_landing:
        instance.remove_from_landing()


@receiver(post_delete, sender=Cup)
def cup_post_delete(sender, instance, **kwargs):
    instance.texture.delete()


@receiver(pre_delete, sender=CupTexture)
def cup_texture_delete(sender, instance, **kwargs):
    instance.file.delete(False)
