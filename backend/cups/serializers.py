from django.http import Http404
from rest_framework import serializers
from cups.models import Cup, CupTexture


class CupTextureSerializer(serializers.ModelSerializer):
    file = serializers.SerializerMethodField('get_file_url')

    class Meta:
        model = CupTexture
        fields = ('id', 'filename', 'file', )

    def get_file_url(self, obj):
        return obj.file.url


class CupSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    uuid = serializers.ReadOnlyField()
    name = serializers.CharField(max_length=255)
    texture_id = serializers.IntegerField()
    texture = CupTextureSerializer(read_only=True)
    show_on_landing = serializers.ReadOnlyField()
    priority = serializers.ReadOnlyField()

    def create(self, validated_data):
        return Cup.objects.create(**validated_data)

    def update(self, instance, validated_data):
        try:
            instance.texture = CupTexture.objects.get(pk=validated_data['texture_id'])
        except:
            raise Http404()

        instance.name = validated_data['name']
        instance.save()
        return instance
