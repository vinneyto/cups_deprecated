
class CupNotOnLandingException(Exception):
    def __init__(self, cup):
        super().__init__('cup width id: {} not on landing'.format(cup.id))


class CupAlreadyOnLandingException(Exception):
    def __init__(self, cup):
        super().__init__('cup width id: {} already on landing'.format(cup.id))
