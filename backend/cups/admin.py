from django.contrib import admin
from django.utils.safestring import mark_safe


from .models import Cup, CupTexture


@admin.register(CupTexture)
class CupTextureAdmin(admin.ModelAdmin):
    fields = ['filename', 'file']


@admin.register(Cup)
class CupAdmin(admin.ModelAdmin):
    readonly_fields = ('cup_link', 'priority', )
    fields = ['name', 'texture', 'cup_link', 'show_on_landing']
    list_display = ('name', 'cup_link', 'show_on_landing', 'texture', )
    list_filter = ('show_on_landing', )
    search_fields = ('name', )
    list_select_related = ('texture', )

    def cup_link(self, instance):
        if instance.pk:
            return mark_safe('<a href="https://cups.vinneyto.org/cup/{}" target="_blank">'
                             'https://cups.vinneyto.org/cup/{}'
                             '</a>'.format(instance.uuid, instance.uuid))
        else:
            return mark_safe('<span>Кружка не сохранена</span>')

    cup_link.short_description = 'Ссылка на кружку'
