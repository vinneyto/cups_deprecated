from django.db import transaction
from rest_framework import serializers

from core.models import Attachment
from core.serializers import AttachmentSerializer
from orders.models import Order


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ('id', 'fullname', 'phone', 'email', 'opened', 'created', 'modified', )


class OrderDetailSerializer(serializers.ModelSerializer):
    attachment_ids = serializers.PrimaryKeyRelatedField(
        many=True,
        read_only=False,
        queryset=Attachment.objects.all(),
        source='attachments'
    )

    attachments = AttachmentSerializer(many=True, read_only=True)

    @transaction.atomic
    def create(self, validated_data):
        attachments = validated_data.pop('attachments')

        order = Order(**validated_data)
        order.save()

        order.attachments.set(attachments)
        order.save()
        return order

    def update(self, instance, validated_data):
        instance.fullname = validated_data.pop('fullname', instance.fullname)
        instance.phone = validated_data.pop('phone', instance.phone)
        instance.email = validated_data.pop('email', instance.email)
        instance.description = validated_data.pop('description', instance.description)
        instance.opened = validated_data.pop('opened', instance.opened)

        if 'attachments' in validated_data:
            instance.attachments.set(validated_data['attachments'])

        instance.save()
        return instance

    class Meta:
        model = Order
        fields = ('id', 'fullname', 'phone', 'email', 'description', 'opened',
                  'created', 'modified', 'attachment_ids', 'attachments', )
