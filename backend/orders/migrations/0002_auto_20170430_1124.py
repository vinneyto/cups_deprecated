# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-30 08:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attachment',
            name='order',
        ),
        migrations.AddField(
            model_name='order',
            name='attachments',
            field=models.ManyToManyField(to='core.Attachment'),
        ),
        migrations.AddField(
            model_name='order',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='Дата создания'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='modified',
            field=models.DateTimeField(auto_now=True, verbose_name='Дата модификации'),
        ),
        migrations.DeleteModel(
            name='Attachment',
        ),
    ]
