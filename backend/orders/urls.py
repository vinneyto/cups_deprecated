from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.OrderListCreateView.as_view()),
    url(r'^(?P<pk>\d+)/$', views.OrderRetrieveUpdateDestroyView.as_view()),

    url(r'^public/$', views.OrderCreateView.as_view()),
]
