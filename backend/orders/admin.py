from django.contrib import admin
from .models import Order


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('fullname', 'phone', 'email', 'opened', 'created', 'modified', )
    search_fields = ('fullname', )
