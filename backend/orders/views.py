from django.db import transaction
from rest_framework.exceptions import APIException, ValidationError, PermissionDenied
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, IsAdminUser, DjangoModelPermissions
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import Attachment
from core.views import ListCreateDetailsAPIView
from core.utils import check_recaptcha_token
from orders.serializers import OrderSerializer, OrderDetailSerializer
from orders.models import Order


# Public views

class OrderCreateView(APIView):
    parser_classes = (MultiPartParser, )

    @transaction.atomic
    def post(self, request):
        data = request.data

        if 'fullname' not in data or 'phone' not in data or 'recaptcha_token' not in data:
            raise ValidationError(detail="Имя, телефон и токен рекапчи должны быть указаны")

        if not check_recaptcha_token(data.get('recaptcha_token')):
            raise PermissionDenied(detail="Валидация рекапчи не пройдена")

        attachments = []
        if 'file' in data:
            for file in data.getlist('file'):
                attachment = Attachment(filename=file.name, file=file)
                try:
                    attachment.save()
                    attachments.append(attachment)
                except Exception as e:
                    raise APIException(detail=str(e))

        order = Order()
        order.fullname = data.get('fullname')
        order.phone = data.get('phone')
        order.email = data.get('email', None)
        order.description = data.get('description', None)
        order.save()

        order.attachments.set(attachments)
        order.save()

        return Response(OrderDetailSerializer(order).data)


# Admin views

class OrderListCreateView(ListCreateDetailsAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser, DjangoModelPermissions, )
    queryset = Order.objects.all()
    list_serializer = OrderSerializer
    detail_serializer = OrderDetailSerializer
    filter_fields = {
        'fullname': ['exact', 'icontains'],
        'phone': ['exact', 'icontains'],
        'email': ['exact', 'icontains'],
        'opened': ['exact'],
        'created': ['exact', 'gte', 'lte']
    }
    ordering_fields = ('fullname', 'phone', 'email', 'created', 'modified', )


class OrderRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser, DjangoModelPermissions, )
    queryset = Order.objects.all().prefetch_related('attachments')
    serializer_class = OrderDetailSerializer
