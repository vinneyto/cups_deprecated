from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from core.models import Attachment


class Order(models.Model):
    fullname = models.CharField(max_length=255, verbose_name="Полное имя")
    phone = models.CharField(max_length=100, verbose_name="Телефон")
    email = models.EmailField(verbose_name="Email", null=True, blank=True)
    description = models.TextField(verbose_name="Описание заказа", null=True, blank=True)
    opened = models.BooleanField(default=False, verbose_name="Просмотрено")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
    modified = models.DateTimeField(auto_now=True, verbose_name="Дата модификации")
    attachments = models.ManyToManyField(Attachment, blank=True)

    def __str__(self):
        return self.fullname

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


@receiver(pre_delete, sender=Order)
def order_pre_delete(sender, instance, **kwargs):
    instance.attachments.all().delete()

