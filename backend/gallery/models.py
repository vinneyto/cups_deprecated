import os
import uuid
from io import BytesIO
from PIL import Image
from datetime import datetime
from django.db import models
from django.db.models.signals import pre_delete, post_delete
from django.dispatch.dispatcher import receiver


def upload_url(obj, filename):
    ext = filename.split('.')[-1]
    return os.path.join('upload/gallery/', '{}.{}'.format(uuid.uuid4().hex, ext))


class GalleryImageFile(models.Model):
    filename = models.CharField(max_length=128)
    file = models.ImageField(upload_to=upload_url, verbose_name="Файл с изображением")
    thumbnail = models.ImageField(upload_to=upload_url, verbose_name="Миниатюра изображения")

    def save(self, *args, **kwargs):
        if self.need_update_thumbnail():
            thumbnail_size = (500, 600)
            image = Image.open(self.file.file)

            image.thumbnail(thumbnail_size, Image.ANTIALIAS)
            image_file = BytesIO()

            image.save(image_file, 'png', quality=90)
            self.thumbnail.save('thumbnail.png', image_file, save=False)

        super().save(*args, **kwargs)

    def need_update_thumbnail(self):
        if self.file is None:
            return False
        elif self.pk is None:
            return True
        else:
            prev_image = GalleryImageFile.objects.get(pk=self.pk)
            if prev_image is None:
                return False
            return prev_image.file.name != self.file.name

    def __str__(self):
        return self.filename

    class Meta:
        verbose_name = 'Файл с изображением'
        verbose_name_plural = 'Файлы с изображением'


class GalleryImage(models.Model):
    name = models.CharField(max_length=255, verbose_name="Имя")
    description = models.CharField(max_length=255, verbose_name="Описание", null=True, blank=True)
    created = models.DateTimeField(default=datetime.now, verbose_name="Дата создания")
    image_file = models.OneToOneField(GalleryImageFile, on_delete=models.CASCADE,
                                      verbose_name='Файл с изображением')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


@receiver(post_delete, sender=GalleryImage)
def gallery_image_post_delete(sender, instance, **kwargs):
    instance.image_file.delete()


@receiver(pre_delete, sender=GalleryImageFile)
def gallery_image_file_pre_delete(sender, instance, **kwargs):
    instance.file.delete(False)
    instance.thumbnail.delete(False)
