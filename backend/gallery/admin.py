from django.contrib import admin
from django.utils.safestring import mark_safe
from django.core import urlresolvers

from .models import GalleryImage, GalleryImageFile
from django import forms


class GalleryImageModelForm(forms.ModelForm):
    class Meta:
        model = GalleryImage
        fields = '__all__'
        widgets = {
            'description': forms.Textarea
        }


@admin.register(GalleryImageFile)
class GalleryImageFileAdmin(admin.ModelAdmin):
    fields = ['filename', 'file', ]


@admin.register(GalleryImage)
class GalleryImageAdmin(admin.ModelAdmin):
    readonly_fields = ('image_thumbnail', 'image_thumbnail_change', )
    fields = ['name', 'image_file', 'created', 'image_thumbnail', 'description', ]
    list_display = ('name', 'created', 'image_thumbnail_change', )
    list_filter = ('created', )
    search_fields = ('name', 'description')
    list_select_related = ('image_file', )
    form = GalleryImageModelForm

    def image_thumbnail(self, instance):
        if instance.pk:
            return mark_safe('<a href="{}" target="_blank">'
                             '<img src="{}" width="200" style="border: 1px solid #aaa">'
                             '</a>'.format(instance.image_file.file.url, instance.image_file.thumbnail.url))
        else:
            return mark_safe('<span>Изображение не сохранено</span>')

    def image_thumbnail_change(self, instance):
        if instance.pk:
            change_url = urlresolvers.reverse('admin:gallery_galleryimage_change', args=(instance.pk,))
            return mark_safe('<a href="{}">'
                             '<img src="{}" width="100" style="border: 1px solid #aaa">'
                             '</a>'.format(change_url, instance.image_file.thumbnail.url))
        else:
            return mark_safe('<span>Изображение не сохранено</span>')

    image_thumbnail.short_description = 'Миниатюра'
    image_thumbnail_change.short_description = 'Миниатюра'
