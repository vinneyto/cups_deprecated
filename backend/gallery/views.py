from rest_framework.exceptions import APIException
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from core.permissions import check_permission
from gallery.models import GalleryImage, GalleryImageFile
from gallery.serializers import GalleryImageSerializer, GalleryImageFileSerializer


# Public view

class GalleryImageListPublicView(ListAPIView):
    queryset = GalleryImage.objects.all().select_related('image_file')
    serializer_class = GalleryImageSerializer
    ordering_fields = ('created', )


# Admin views

class GalleryImageListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser, DjangoModelPermissions, )
    queryset = GalleryImage.objects.all().select_related('image_file')
    serializer_class = GalleryImageSerializer
    filter_fields = {
        'name': ['exact', 'icontains'],
        'created': ['exact', 'gte', 'lte']
    }
    ordering_fields = ('created', 'name', )


class GalleryImageRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser, DjangoModelPermissions, )
    queryset = GalleryImage.objects.all().select_related('image_file')
    serializer_class = GalleryImageSerializer


class GalleryImageFileCreateView(APIView):
    parser_classes = (MultiPartParser,)
    permission_classes = (IsAuthenticated, IsAdminUser, )

    @check_permission((GalleryImageFile, 'add'))
    def post(self, request):
        file = request.data['file']

        image_file = GalleryImageFile(filename=file.name, file=file)
        try:
            image_file.save()
        except Exception as e:
            raise APIException(detail=str(e))

        return Response(GalleryImageFileSerializer(image_file).data)
