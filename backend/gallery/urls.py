from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.GalleryImageListCreateView.as_view()),
    url(r'^(?P<pk>\d+)/$', views.GalleryImageRetrieveUpdateDestroyView.as_view()),
    url(r'^public/$', views.GalleryImageListPublicView.as_view()),

    url(r'^image/$', views.GalleryImageFileCreateView.as_view()),
]
