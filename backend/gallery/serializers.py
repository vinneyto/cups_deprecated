from django.http import Http404
from rest_framework import serializers
from gallery.models import GalleryImage, GalleryImageFile


class GalleryImageFileSerializer(serializers.ModelSerializer):
    file = serializers.SerializerMethodField('get_file_url')
    thumbnail = serializers.SerializerMethodField('get_thumbnail_url')

    class Meta:
        model = GalleryImageFile
        fields = ('id', 'filename', 'file', 'thumbnail', )

    def get_file_url(self, obj):
        return obj.file.url

    def get_thumbnail_url(self, obj):
        return obj.thumbnail.url


class GalleryImageSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.CharField(max_length=255)
    description = serializers.CharField(max_length=255, allow_blank=True, allow_null=True)
    created = serializers.ReadOnlyField()
    image_file_id = serializers.IntegerField()
    image_file = GalleryImageFileSerializer(read_only=True)

    def create(self, validated_data):
        return GalleryImage.objects.create(**validated_data)

    def update(self, instance, validated_data):
        try:
            instance.image_file = GalleryImageFile.objects.get(pk=validated_data['image_file_id'])
        except:
            raise Http404()

        instance.name = validated_data['name']
        instance.description = validated_data['description']
        instance.save()
        return instance

